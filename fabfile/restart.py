from fabric.api import run, hosts, task, settings, env
from fetch_servers import fetch_servers

servers = fetch_servers()
env.hosts = servers.keys()

@task
def map_updaters():
    with settings(warn_only=True):
        run('docker kill map_updater')

@task
@hosts('dev1.tempus.xyz')
def dev_map_updater():
	with settings(warn_only=True):
		run('docker kill map_updater')

@task
def queue_all():
    with settings(warn_only=True):

        # container = { 'shortname': Bool }
        for container in servers[env.host_string]:
            for value in container:
                # Players in server
                if container[value]:
                    run('docker exec tf2_{0} wget --post-data '' http://localhost:8080/restart/mapchange'.format(value))

                # (probably) safe to exit
                else:
                    run('docker kill tf2_{0}'.format(value))