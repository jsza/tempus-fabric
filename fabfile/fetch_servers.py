import urllib2
import json

special_connect_strings = {
    'SG': 'sql.asiafortress.com:18002',
    'RU': 'ru1.tempus.xyz:61722'
}


def fetch_servers():
    server_status = urllib2.urlopen("https://tempus.xyz/api/servers/statusList").read()
    tempus_servers = json.loads(server_status)

    server_list = dict()

    for server in tempus_servers:
        if not server['server_info']['hidden']:
            """
            {
                "hostname": [
                    { shortname: True if players else False },
                    ...
                ],
                ...
            }
            """
            # remove numbers so we are left with just the shortname as defined in special_connect_strings
            short = ''.join([i for i in server['server_info']['shortname'] if not i.isdigit()])
            if short in special_connect_strings.keys():
                server_list.setdefault(special_connect_strings[short], []).append(
                    {server['server_info']['shortname']: bool(server['game_info']['users'])}
                )
            else:
                server_list.setdefault(server['server_info']['addr'] + ':22', []).append(
                    {server['server_info']['shortname']: bool(server['game_info']['users'])}
                )

    return server_list