from fabric.api import run, hosts, task, settings, env
from fetch_servers import fetch_servers

servers = fetch_servers()
env.hosts = servers.keys()

@task
def all():
        env.user = 'steam-docker'
        for container in servers[env.host_string]:
            for value in container:
                with settings(warn_only=True):
                    run('cd /srv/tf2_{0}/tf/addons/sourcemod/plugins/disabled/tempus-sourcemod-plugins && git pull'.format(value))
